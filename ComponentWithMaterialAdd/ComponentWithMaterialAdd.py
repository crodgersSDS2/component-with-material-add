from Component import Component
from sds2.utility.gadget_protocol import GadgetComponent
from Versions import ComponentWithMaterialAddVersions
from Screen import BuildComponentUI
from Point3D import Point3D
from point import Point
from Transform3D import MemberTransform, GetXYZ
from MemberBase import ReuseAllMaterialByCreator
import model
import math
from rect_plate import RectPlate
from member import Member
from job import Job
from shape import Shape

@ComponentWithMaterialAddVersions
class ComponentWithMaterialAdd(GadgetComponent, Component):
    """
    Include a brief description of the component
    """

    def __init__(self, **kwargs):
        ComponentWithMaterialAddVersions.init_factory()(self, **kwargs) #  run the init from the
                                             #  versioning class
        Component.__init__(self, **kwargs)


    def IsAllowedOnMember(self, mn):
        """Determine whether or not the component is allowed on the member
        
        This method will be called in modeling whenever a component add
        is started. It will control whether or not a member is allowed to be
        chosen; it will also determine which components are shown in the 
        component add list if members are preselected.

        Args:
            mn (int): Member number of the member being considered
        
        Returns:
            bool: True if the component can be added to the member
                  False otherwise
        """
        return model.member(mn).type == model.Beam


    def SetReferencePointForMemberUI(self, mn):
        """Set the reference point for the component on the member

        The reference point is relative to the left end of the host member
        
        Args:
            mn (int): Member number of the member hosting the component
        """

        # because the point is relative, (0., 0., 0.) will return the 
        # left end of the host member as the reference point for the component
        self.ref_point = Point3D(0., 0., 0.)

        # this can be left out if the left end of the member is acceptable
        # as the reference point

    def Add(self, mn):
        """Take care of any user input needed to add the component
        
        Args:
            mn (int): Member number of the member hosting the component
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        # take care of any necessary user input here, e.g. point location
        return True
    
    def Modifies(self):
        """Returns a list of member numbers that the component will modify

        Returns:
            list: List of member numbers
        """
    
        #the component will always modifiy its host
        return [self.member_number, ]

    @classmethod
    def Factory(cls, host, member_universe):
        """Instantiate, initialize, and return an instance of the component
        
        Args:
            cls (ComponentWithMaterialAdd): the component class
            host (int): Member number for the component host
            member_universe (list): List of members related to the component
        
        Returns:
            Component: An instantiated version of the component class
        """
        comp = cls()  # create an instance of the component
        # Set any attributes
        return comp

    # @staticmethod is required for this method
    # this is part of the Gadget Protocol interface
    @staticmethod
    def CreateCustomMultiEditableUI(model, gadget_factory):
        """Create the UI for the component
        
        Args:
            model (list): List of component objects considered for edit
            gadget_factory (GadgetFactory): Convenience class for creating
                                            items in the screen
        """
        BuildComponentUI(model, gadget_factory)

    def Design(self):
        """Any calculations to be done and values to be set should be done here.
        The values set here are available through the rest of the process hooks.
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        beam_left = Point3D(model.member(self.member_number).ends[0].location)
        beam_right = Point3D(model.member(self.member_number).ends[1].location)
        
        beam_length = beam_left.Distance(beam_right)
        
        self.length = beam_length / 2.
        
        beam = model.member(self.member_number)
        self.width = Shape(beam.section_size).flange_width

        return True
    
    def CreateMaterial(self):
        """Material created by the component for its host member is created here
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """

        beam = model.member(self.member_number)
        beam_width = Shape(beam.section_size).flange_width
        beam_left = Point3D(beam.ends[0].location)
        beam_right = Point3D(beam.ends[1].location)
        
        member_xform = MemberTransform(beam_left, beam_right, beam.rotation)
        x, y, z = GetXYZ(member_xform)

        plate_left = (
            beam_left +
            (y * (self.thickness / 2.)) +
            z * (beam_width / 2.)
        )
        plate_right = plate_left + (x * self.length)

        plate = RectPlate()
        plate.Member = Member(self.member_number)
        plate.pt1 = Point(plate_left)
        plate.pt2 = Point(plate_right)
        plate.grade = 'A36'
        plate.origin = "Center"

        plate.top_oper_left = 'None'
        plate.top_oper_right = 'None'
        
        plate.top_clip_left = 0.
        plate.top_length_left = 0.
        plate.top_cope_left = 0.
        plate.top_length_left = 0.

        plate.top_clip_right = 0.
        plate.top_length_right = 0.
        plate.top_cope_right = 0.
        plate.top_length_right = 0.

        plate.bottom_oper_left = "None"
        plate.bottom_oper_right = "None"
        plate.width = beam_width
        plate.thick = self.thickness
        plate.work_pt_dist = plate_left.Distance(plate_right)
        plate.setback_left = 0.
        plate.setback_right = 0.
        plate.web_cut_angle_left = 0.
        plate.web_cut_angle_right = 0.
        plate.length = plate.work_pt_dist
        plate.mtrl_type = "Plate"
        plate.mtrl_usage = "Example Component Material"
        plate.finish = 'None'
        plate.MaterialColor3d = Job().default_colors()[6]  # index for 'Medium_Material'
        plate.ref_pt_offset = (0.000000, 0.000000, 0.000000)
        plate.add()
        plate_xform = MemberTransform(
            plate_left, plate_right, beam.rotation
        )
        
        plate_xform.RotateX(math.radians(90.))
        plate.SetTransform(plate_xform)
        return plate
    
    def CreateMaterialOther(self, other_member):
        """Material Created by the component for a member that does not host the
        component is created here

        Args:
            other_member (int): Member number of the member to which material is
            being added
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        return True
    
    def CreateHoleMatch(self):
        """Create holes between material and the host member
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        return True
    
    def CreateHoleMatchOther(self, other_member):
        """Create holes between material and members not hosting the component.

        Args:
            other_member (int): Member to which holes are being added
            
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        return True
        
    def CreateDependentMaterial(self):
        """Create bolts, welds and cuts on the host member
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        return True
    
    def CreateDependentMaterialOther(self, other_member):
        """Create bolts, welds and cuts on the host member
        
        Args:
            other_member (int): Member to which dependent materials are being
            added
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        return True
    
    def CreateViews(self):
        """Create custom views on the host member for the component
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        return True
    
    def ProcessFinal(self):
        """Any last minte cleanup for the host member
        
        NOTE:
        -----
        This can often be left default
        
        Returns:
            bool: True allows the component to continue; False will stop the
                  process of adding the component
        """
        
        # attempts to reuse guids for material created by the component
        ReuseAllMaterialByCreator(self.member_number, self.uuid)
