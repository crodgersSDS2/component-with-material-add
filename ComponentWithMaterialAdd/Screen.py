from sds2.utility.gadget_protocol import SubdialogController

from dialog.checkbox import Checkbox
from dialog.dimension import DimensionStyled
from dialog.entry import Entry


def MainLeaf(parent):
    # Entry(parent, 'width', DimensionStyled(), 'width:')
    Entry(parent, 'thickness', DimensionStyled(), 'thickness:')


def BuildComponentUI(model, gadget_factory):
    main_controller = SubdialogController(model)
    main_column = gadget_factory.LoadSaveColumn(
        main_controller,
        None,  # callback method, usually not necessary for columns
        'Column Title',
        '',  #sort name
        'unique-fold-id',  # remembers fold state after edit
        'unique-form-name',
        defaultfold=True  # default state is open; defaults to False
    )

    
    gadget_factory.Leaf(
        main_column,
        main_controller,
        MainLeaf,  # no () here
        'leaf name',
        '',  # sort name
        'unique-fold-id',
        defaultfold=True
    )

