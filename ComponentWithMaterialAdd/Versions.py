from sds2.componentmetamorphoses import ComponentMetamorphoses
from metamorphoses import Version, Attr

class ComponentWithMaterialAddVersions(ComponentMetamorphoses):
    versions = {}
    versions[0] = Version(
        Attr('width', 12.),
        Attr('length', 18.),
        Attr('thickness', .25)
    )